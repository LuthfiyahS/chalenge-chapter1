const axios = require('axios') 
getDataAsyncronous();
//getDataCallback();
 async function getDataAsyncronous(){
     try {
         const response = await axios('https://indonesia-public-static-api.vercel.app/api/heroes')
         const data = response.data;
         console.log(data[0])
         console.log(`${data[0].name} lahir tahun ${data[0].birth_year} `)
     } catch (err) {
         console.log(err.message)
     }
 }

 function getDataCallback(){
     axios.get('https://indonesia-public-static-api.vercel.app/api/heroes')
     .then(response=>{
         console.log(response.data)
     }).catch(err=>{
         console.error(err)
     })
 }